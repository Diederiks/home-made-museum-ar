﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class ImageRecognitionClass : MonoBehaviour {

    public static ImageRecognitionClass instance;
    ARTrackedImage _ARTrackedImage;
    [SerializeField] Text nameText;

    //These are all the statues that get displayed upon answering the quiz correctly.
    public GameObject _bloodyStatue;
    public GameObject _burningStatue;
    public GameObject _drowningStatue;
    public GameObject _flowerStatue;
    public GameObject _alphaStatue;
    public GameObject _betaStatue;
    public GameObject _charlieStatue;
    public GameObject _deltaStatue;
    public GameObject _echoStatue;
    public GameObject _foxtrotStatue;

    //These bools are used to inform the Quiz Manager script of the image recognition status.
    public bool found1 = false;
    public bool found2 = false;
    public bool found3 = false;
    public bool found4 = false;
    public bool found5 = false;
    public bool found6 = false;
    public bool found7 = false;
    public bool found8 = false;
    public bool found9 = false;
    public bool found10 = false;

    //In the start we set all the statues to inactive.
    private void Start () {
        instance = this;
        _bloodyStatue.SetActive (false);
        _burningStatue.SetActive (false);
        _drowningStatue.SetActive (false);
        _flowerStatue.SetActive (false);
        _alphaStatue.SetActive (false);
        _betaStatue.SetActive (false);
        _charlieStatue.SetActive (false);
        _deltaStatue.SetActive (false);
        _echoStatue.SetActive (false);
        _foxtrotStatue.SetActive (false);
        _ARTrackedImage = GetComponent<ARTrackedImage> ();
    }

    private void Update () {
        CheckName (_ARTrackedImage.referenceImage.name);
    }

    //In this fuction we check the name of the recognized image, set the found bool to true and call a fuction from the Quiz Manager script.
    void CheckName (string name) {
        nameText.text = name;

        if (name == "Bloody") {
            found1 = true;
            QuizManager.instance.Bloody ();
        }

        if (name == "Burning") {
            found2 = true;
            QuizManager.instance.Burning ();
        }

        if (name == "Drowning") {
            found3 = true;
            QuizManager.instance.Drowning ();
        }

        if (name == "Flower") {
            found4 = true;
            QuizManager.instance.Flower ();
        }

        if (name == "Alpha") {
            found5 = true;
            QuizManager.instance.Alpha ();
        }

        if (name == "Beta") {
            found6 = true;
            QuizManager.instance.Beta ();
        }

        if (name == "Charlie") {
            found7 = true;
            QuizManager.instance.Charlie ();
        }

        if (name == "Delta") {
            found8 = true;
            QuizManager.instance.Delta ();
        }

        if (name == "Echo") {
            found9 = true;
            QuizManager.instance.Echo ();
        }

        if (name == "Foxtrot") {
            found10 = true;
            QuizManager.instance.Foxtrot ();
        }

        //With the following statements we check the names and wether the correct answer is chosen before we set the corresponding statue to true.
        if (name == "Bloody" && QuizManager.instance.thisIsIt1) {
            _bloodyStatue.SetActive (true);
            QuizManager.instance._ArtistDetails1.SetActive(true);
        }

        if (name == "Burning" && QuizManager.instance.thisIsIt2) {
            _burningStatue.SetActive (true);
            QuizManager.instance._ArtistDetails2.SetActive(true);
        }

        if (name == "Drowning" && QuizManager.instance.thisIsIt3) {
            _drowningStatue.SetActive (true);
            QuizManager.instance._ArtistDetails3.SetActive(true);
        }

        if (name == "Flower" && QuizManager.instance.thisIsIt4) {
            _flowerStatue.SetActive (true);
            QuizManager.instance._ArtistDetails4.SetActive(true);
        }

        if (name == "Alpha" && QuizManager.instance.thisIsIt5) {
            _alphaStatue.SetActive (true);
            QuizManager.instance._ArtistDetails5.SetActive(true);
        }

        if (name == "Beta" && QuizManager.instance.thisIsIt5) {
            _betaStatue.SetActive (true);
            QuizManager.instance._ArtistDetails6.SetActive(true);
        }

        if (name == "Charlie" && QuizManager.instance.thisIsIt5) {
            _charlieStatue.SetActive (true);
            QuizManager.instance._ArtistDetails7.SetActive(true);
        }

        if (name == "Delta" && QuizManager.instance.thisIsIt5) {
            _deltaStatue.SetActive (true);
            QuizManager.instance._ArtistDetails8.SetActive(true);
        }

        if (name == "Echo" && QuizManager.instance.thisIsIt5) {
            _echoStatue.SetActive (true);
            QuizManager.instance._ArtistDetails9.SetActive(true);
        }

        if (name == "Foxtrot" && QuizManager.instance.thisIsIt5) {
            _foxtrotStatue.SetActive (true);
            QuizManager.instance._ArtistDetails10.SetActive(true);
        }
    }
}
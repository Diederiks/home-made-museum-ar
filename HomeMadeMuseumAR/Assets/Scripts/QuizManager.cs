﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour {
    public static QuizManager instance;

    //Bools to indicate to the ImageRecognitionClass script that the right quiz answeres has been picked.
    public bool thisIsIt1 = false;
    public bool thisIsIt2 = false;
    public bool thisIsIt3 = false;
    public bool thisIsIt4 = false;
    public bool thisIsIt5 = false;
    public bool thisIsIt6 = false;
    public bool thisIsIt7 = false;
    public bool thisIsIt8 = false;
    public bool thisIsIt9 = false;
    public bool thisIsIt10 = false;

    //These are the quiz panels that house the individual questions and answer buttons.
    public GameObject _canvas1;
    public GameObject _canvas2;
    public GameObject _canvas3;
    public GameObject _canvas4;
    public GameObject _canvas5;
    public GameObject _canvas6;
    public GameObject _canvas7;
    public GameObject _canvas8;
    public GameObject _canvas9;
    public GameObject _canvas10;

    //These are the information panels on the artist and artwork.
    public GameObject _ArtistDetails1;
    public GameObject _ArtistDetails2;
    public GameObject _ArtistDetails3;
    public GameObject _ArtistDetails4;
    public GameObject _ArtistDetails5;
    public GameObject _ArtistDetails6;
    public GameObject _ArtistDetails7;
    public GameObject _ArtistDetails8;
    public GameObject _ArtistDetails9;
    public GameObject _ArtistDetails10;

    //In the start we set all canvasas to inactive.
    void Start () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
        instance = this;
    }

    //In the update we check whther or not the ImageRecognition recognises the images and sets the found bools true.
    private void Update () {
        if (ImageRecognitionClass.instance.found1 == true) {
            Bloody ();
        }
        if (ImageRecognitionClass.instance.found2 == true) {
            Burning ();
        }
        if (ImageRecognitionClass.instance.found3 == true) {
            Drowning ();
        }
        if (ImageRecognitionClass.instance.found4 == true) {
            Flower ();
        }
        if (ImageRecognitionClass.instance.found5 == true) {
            Alpha ();
        }
        if (ImageRecognitionClass.instance.found6 == true) {
            Beta ();
        }
        if (ImageRecognitionClass.instance.found7 == true) {
            Charlie ();
        }
        if (ImageRecognitionClass.instance.found8 == true) {
            Delta ();
        }
        if (ImageRecognitionClass.instance.found9 == true) {
            Echo ();
        }
        if (ImageRecognitionClass.instance.found10 == true) {
            Foxtrot ();
        }
    }

    //All the voids that ends in right ensures that all the canvases are turned of except the relevant Artist detail canvas.

    public void BloodyRight () {
        thisIsIt1 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (true);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void BurningRight () {
        thisIsIt2 = true;
        _ArtistDetails2.SetActive (true);
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void DrowningRight () {
        thisIsIt3 = true;
        _ArtistDetails3.SetActive (true);
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void FlowerRight () {
        thisIsIt4 = true;
        _ArtistDetails4.SetActive (true);
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }

    public void AlphaRight () {
        thisIsIt5 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (true);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void BetaRight () {
        thisIsIt6 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (true);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void CharlieRight () {
        thisIsIt7 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (true);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void DeltaRight () {
        thisIsIt8 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (true);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void EchoRight () {
        thisIsIt9 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (true);
        _ArtistDetails10.SetActive (false);
    }
    public void FoxtrotRight () {
        thisIsIt10 = true;
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (true);
    }

    //These voids are all used to make sure the correct quiz canvas is desplayed with the correct recognized image.

    public void Bloody () {
        _canvas1.SetActive (true);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);

    }
    public void Burning () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (true);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }

    public void Drowning () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (true);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }

    public void Flower () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (true);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }

    public void Alpha () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (true);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void Beta () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (true);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void Charlie () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (true);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void Delta () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (true);
        _canvas9.SetActive (false);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails9.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void Echo () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (true);
        _canvas10.SetActive (false);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails10.SetActive (false);
    }
    public void Foxtrot () {
        _canvas1.SetActive (false);
        _canvas2.SetActive (false);
        _canvas3.SetActive (false);
        _canvas4.SetActive (false);
        _canvas5.SetActive (false);
        _canvas6.SetActive (false);
        _canvas7.SetActive (false);
        _canvas8.SetActive (false);
        _canvas9.SetActive (false);
        _canvas10.SetActive (true);
        _ArtistDetails1.SetActive (false);
        _ArtistDetails2.SetActive (false);
        _ArtistDetails3.SetActive (false);
        _ArtistDetails4.SetActive (false);
        _ArtistDetails5.SetActive (false);
        _ArtistDetails6.SetActive (false);
        _ArtistDetails7.SetActive (false);
        _ArtistDetails8.SetActive (false);
        _ArtistDetails9.SetActive (false);
    }
}
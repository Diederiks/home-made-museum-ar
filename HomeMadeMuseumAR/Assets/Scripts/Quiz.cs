﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quiz : MonoBehaviour {
    public static Quiz instance;
    public GameObject _Prefab;
    public Button _RA;
    public Button _WA1;
    public Button _WA2;
    public bool thisIsIt = false;
    public GameObject QuizCanvas;
    public GameObject otherQuiz1;
    public GameObject otherQuiz2;
    public GameObject otherQuiz3;

    public GameObject ArtworkDetails;

    void Start () {
        otherQuiz1.SetActive (false);
        otherQuiz2.SetActive (false);
        otherQuiz3.SetActive (false);
        QuizCanvas.SetActive (false);
        ArtworkDetails.SetActive (false);
        instance = this;
        _RA.onClick.AddListener (RightAnswer);
        _WA1.onClick.AddListener (WrongAnswer);
        _WA2.onClick.AddListener (WrongAnswer);
    }

    public void RightAnswer () {
        thisIsIt = true;
        QuizCanvas.SetActive (false);
        ArtworkDetails.SetActive (true);
    }

    public void WrongAnswer () {
        QuizCanvas.SetActive (true);
        otherQuiz1.SetActive (false);
        otherQuiz2.SetActive (false);
        otherQuiz3.SetActive (false);
    }

    public void WheresWaldo () {
        QuizCanvas.SetActive (true);
        otherQuiz1.SetActive (false);
        otherQuiz2.SetActive (false);
        otherQuiz3.SetActive (false);
    }
}
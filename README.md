# Home Made Museum AR

# Introduction
Home made museum uses AR image tracing software to give the user a interactive experience with art. The user opens the application, scans an image to answer a trivia question about art. If the user answers correctly a statue will be shown.

# Requirements
 - a mobile android device that is AR core compatible.
   
 - Provided images (10).

# Instructions
 - Upload the latest build to your mobile device or android device.
   
 - Scan one of the given images.
         
 - Answer the question.
             
 - See the artwork.

# Uses
 You can use it to make an art expedition more interactive.
 For educational purposes, to teach people more about art.
 Can be used for scavenger hunts.
 Can be used for quiz cometitions.

# Tutorials used
I used the video provided by my lecturer Jared Brandjes.

